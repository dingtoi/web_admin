export default {
	isDesktop: undefined,
	isTablet: 'tablet',
	isMobile: 'mobile',
	limit: 10,
	order: {
		created: 'CREATED',
		group: 'GROUP',
		inTransaction: 'IN_TRANSACTION',
		ownerScanned: 'OWNER_SCANNED',
		buyerRejected: 'BUYER_REJECTED',
		buyerAccept: 'BUYER_ACCEPT',
		pending: 'PENDING'
	},
};
