import axios from 'axios';

export const listColor = (domain, { token, limit, offset }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'colors',
				{ limit, offset },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const detailColor = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.get(
				domain + 'color/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const addColor = (domain, { token, name, colorCode }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'color',
				{ name, colorCode },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const editColor = (domain, { token, id, name, colorCode }) => {
	return new Promise((resolve, reject) => {
		axios
			.put(
				domain + 'color/' + id,
				{ name, colorCode },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const deleteColor = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.delete(
				domain + 'color/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const listCapacity = (domain, { token, limit, offset }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'capacities',
				{ limit, offset },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const detailCapacity = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.get(
				domain + 'capacity/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const addCapacity = (domain, { token, value }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'capacity',
				{ value },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const editCapacity = (domain, { token, id, value }) => {
	return new Promise((resolve, reject) => {
		axios
			.put(
				domain + 'capacity/' + id,
				{ value },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const deleteCapacity = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.delete(
				domain + 'capacity/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const listRam = (domain, { token, limit, offset }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'rams',
				{ limit, offset },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const detailRam = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.get(
				domain + 'ram/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const addRam = (domain, { token, value }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'ram',
				{ value },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const editRam = (domain, { token, id, value }) => {
	return new Promise((resolve, reject) => {
		axios
			.put(
				domain + 'ram/' + id,
				{ value },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const deleteRam = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.delete(
				domain + 'ram/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const listBrand = (domain, { token, limit, offset }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'brands',
				{ limit, offset },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const detailBrand = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.get(
				domain + 'brand/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const addBrand = (domain, { token, name, imageUrl }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'brand',
				{ name, imageUrl },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const editBrand = (domain, { token, id, name, imageUrl }) => {
	return new Promise((resolve, reject) => {
		axios
			.put(
				domain + 'brand/' + id,
				{ name, imageUrl },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const deleteBrand = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.delete(
				domain + 'brand/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const listCategory = (domain, { token, limit, offset }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'categories',
				{ limit, offset },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const detailCategory = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.get(
				domain + 'category/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const addCategory = (domain, { token, name, imageUrl }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'category',
				{ name, imageUrl },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const editCategory = (domain, { token, id, name, imageUrl }) => {
	return new Promise((resolve, reject) => {
		axios
			.put(
				domain + 'category/' + id,
				{ name, imageUrl },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const deleteCategory = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.delete(
				domain + 'category/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const listModel = (domain, { token, limit, offset, filter }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'models',
				{ limit, offset, filter },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const detailModel = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.get(
				domain + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const addModel = (domain, { token, name, imageUrl, brandId, categoryId }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain,
				{ name, imageUrl, brandId, categoryId },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const editModel = (domain, { token, id, name, imageUrl, brandId, categoryId }) => {
	return new Promise((resolve, reject) => {
		axios
			.put(
				domain + id,
				{ name, imageUrl, brandId, categoryId },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const deleteModel = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.delete(
				domain + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const listImei = (domain, { token, limit, offset, filter }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'imeis',
				{ limit, offset, filter },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const detailImei = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.get(
				domain + 'imei/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const addImei = (domain, { token, imei, modelId, colorId, ramId, capacityId, otherDetail }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'imei',
				{ imei, modelId, colorId, ramId, capacityId, otherDetail },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const editImei = (domain, { token, id, imei, modelId, colorId, ramId, capacityId, otherDetail }) => {
	return new Promise((resolve, reject) => {
		axios
			.put(
				domain + 'imei/' + id,
				{ imei, modelId, colorId, ramId, capacityId, otherDetail },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const deleteImei = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.delete(
				domain + 'imei/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const listAccessory = (domain, { token, limit, offset }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'accessories',
				{ limit, offset },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const detailAccessory = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.get(
				domain + 'accessory/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const addAccessory = (domain, { token, value, key, order }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'accessory',
				{ value, key, order },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const editAccessory = (domain, { token, id, value, key, order }) => {
	return new Promise((resolve, reject) => {
		axios
			.put(
				domain + 'accessory/' + id,
				{ value, key, order },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const deleteAccessory = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.delete(
				domain + 'accessory/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const listSetting = (domain, { token, limit, offset }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'settings',
				{ limit, offset },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const detailSetting = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.get(
				domain + 'setting/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const addSetting = (domain, { token, value, key }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'setting',
				{ value, key },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
export const editSetting = (domain, { token, id, value, key }) => {
	return new Promise((resolve, reject) => {
		axios
			.put(
				domain + 'setting/' + id,
				{ value, key },
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};

export const deleteSetting = (domain, { token, id }) => {
	return new Promise((resolve, reject) => {
		axios
			.delete(
				domain + 'setting/' + id,
				{
					timeout: 20000,
					headers: {
						Authorization: 'Bearer ' + token,
					},
				}
			)
			.then((response) => {
				const { obj } = response.data;
				resolve(obj);
			})
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status === 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
