import axios from 'axios';

export const login = (domain, { email, password }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(
				domain + 'loginAdmin',
				{ email, password },
				{
					timeout: 10000,
				}
			)
			.then(resolve)
			.catch(reject);
	});
};

export const logout = (domain, { token }) => {
	return new Promise((resolve, reject) => {
		axios
			.post(domain + 'logoutAdmin', null, {
				timeout: 10000,
				headers: {
					Authorization: 'Bearer ' + token,
				},
			})
			.then(resolve)
			.catch((error) => {
				let flag = true;
				if (error.response) {
					const { status } = error.response;
					if (status == 401) {
						flag = false;
						auth.set(null);
						location.reload();
					}
				}
				if (flag) {
					reject(error);
				}
			});
	});
};
