import { format as dateFormat } from 'date-fns';
import voca from 'voca';

export const extractErrors = ({ inner }) => {
	return inner.reduce((acc, err) => {
		return { ...acc, [err.path]: err.message };
	}, {});
};

export const extractError = (error, field) => {
	return error.message;
};

export const extractNameFromEmail = (email) => {
	if (email) {
		const arrSplit = email.split('@');
		return voca.truncate(arrSplit[0], 12);
	}
	return 'noname';
};
export const displayDateDefault = (date) => {
	return dateFormat(new Date(date), 'dd/MM/yyyy');
};
