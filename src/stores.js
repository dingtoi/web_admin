import { writable as localWritable } from 'svelte/store';

// export const currency = writable('currency', 'USD');
// export const auth = writable('auth', null);
// export const anonymous = writable('anonymous', null);
// export const isVendor = writable('isVendor', false);

export const fieldCapacity = localWritable('');
export const fieldRam = localWritable('');
export const fieldColor = localWritable('');
export const fieldPhysicalGrading = localWritable('');

export const infoImei = localWritable(null);
export const selectedModel = localWritable(null);
export const selectedExchangeDevice = localWritable(null);

export const selectedAccountItem = localWritable(null);

export const linkToBack = localWritable('');

export const cartChanged = localWritable(0);
export const favoriteChanged = localWritable(0);

export const category = localWritable(null);

export const isAuth = () => {
	const auth = localStorage.getItem('auth');
	if (!auth) {
		return false;
	}
	return true;
};

export const setAuth = (obj) => {
	const objStr = JSON.stringify(obj);
	localStorage.setItem('auth', objStr);
};

export const removeAuth = () => {
	localStorage.removeItem('auth');
};

export const getEmail = () => {
	const auth = localStorage.getItem('auth');
	if (!auth) return '';
	const authObj = JSON.parse(auth);
	if (!authObj.email) return '';
	return authObj.email;
};

export const getToken = () => {
	const auth = localStorage.getItem('auth');
	if (!auth) return '';
	const authObj = JSON.parse(auth);
	if (!authObj.accessToken) return '';
	return authObj.accessToken;
};

